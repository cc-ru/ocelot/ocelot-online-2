ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"

lazy val brain = ProjectRef(file("lib/ocelot-brain"), "ocelot-brain")
lazy val root = (project in file("."))
  .settings(
    name := "ocelot-online-2",
    idePackagePrefix := Some("ocelot.online")
  )
  .dependsOn(brain % "compile->compile")
  .aggregate(brain)

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
assembly / assemblyJarName := "ocelot-online-2.jar"

libraryDependencies += "io.helidon.webserver" % "helidon-webserver" % "4.0.0"
