## Ocelot Online 2
Online emulator for OpenComputers.  
Based on `ocelot-brain` library to do all the emulation work under the hood.

### Demo
WIP

### Build instructions
To build from source and start Ocelot Online 2, type:

```sh
$ sbt run
```

If you want to get a JAR, use this:

```sh
$ sbt assembly
```

**P.S.** This project is a *work-in-progress*, breaking changes and bugs
will happen from time to time. Beware.
