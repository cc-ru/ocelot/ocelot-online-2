package ocelot.online

import io.helidon.webserver.http.{HttpFeature, HttpRouting}
import totoro.ocelot.brain.Ocelot

import java.nio.file.Paths

class BrainFeature extends HttpFeature {
  override def setup(routing: HttpRouting.Builder): Unit = {
    println("Setting up Ocelot Brain...")
    Ocelot.configPath = Some(Paths.get("."))
    Ocelot.librariesPath = Some(Paths.get("."))
    Ocelot.isPlayerOnlinePredicate = Some(_ => true)
    Ocelot.initialize()
  }

  override def afterStop(): Unit = {
    println("Stopping Ocelot Brain...")
    Ocelot.shutdown()
  }
}
