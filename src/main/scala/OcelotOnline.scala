package ocelot.online

import io.helidon.config.Config
import io.helidon.logging.common.LogConfig
import io.helidon.webserver.WebServer
import io.helidon.webserver.http.HttpRouting

object OcelotOnline {
  def main(args: Array[String]): Unit = {
    // load logging configuration
    LogConfig.configureRuntime()

    // initialize global config from default configuration
    val config = Config.create()
    Config.global(config)

    //noinspection ConvertibleToMethodValue
    val server = WebServer.builder()
      .config(config.get("server"))
      .routing(OcelotOnline.routing(_))
      .build()
      .start()

    println("Ocelot server is up! http://localhost:" + server.port())
  }

  private def routing(routing: HttpRouting.Builder): Unit = {
    routing.addFeature(() => new BrainFeature)
  }
}
